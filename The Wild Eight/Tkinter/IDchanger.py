# -*- coding: utf-8 -*-
import Tkinter as tk
import os
import base64
from DJ_ico import img as icon_img

Game = u'The Wild Eight(荒野八人組)'
FileName = u'CONFIG.ini'
KEY = 'UserName'

fail = False
bufa = ''
bufb = ''
def GetFileName():
    #Read the ini file to get the player name
    #And full the buffer a and b
    global bufa , bufb
    if not os.path.isfile("./"+FileName):
        raise IOError('File not found.')
    config = open(FileName, "r")
    success = False
    while True:
        line = config.readline()
        if not line:
            break
        elif line[:len(KEY)]==KEY:
            name = line[len(KEY)+1:len(line)-1]
            success = True
        elif success:
            bufb += line
        else:
            bufa += line
    config.close()
    if success:
        return name
    else:
        raise ValueError('KEY error : '+KEY)

def clickOK():
    if(fail):
        return
    lab_success.pack(padx=5,side='left')
    
    try:
        name = entry_username.get()
        if(name==''):
            raise IOError('User Name can not be empty.')
        WriteName(name)
        lab_success.configure(text=u"修改成功",fg='green')
        lab_info.configure(text="ID: "+name,fg='black')
    except Exception,e:
        lab_success.configure(text=u"Failed",fg='red')
        lab_info.configure(text=u"log: "+str(e),fg='red')
        
def WriteName(name):
    config = open(FileName, "w")
    config.write(bufa)
    config.write(KEY+'=')
    try:
        config.write(name.encode('gb2312'))
    except Exception,e:
        print 'Exception:',e
        config.write('DJoke')
    config.write('\n')
    config.write(bufb)
    config.close()
    
win = tk.Tk()
win.title("遊戲ID修改器")   # 設定標題
win.resizable(0,0)  # 禁止使用者調整大小

'''宣告物件'''
lab_game = tk.Label(win,text=u"Game : "+Game,anchor='w')

lab_username = tk.Label(win, text=u"User Name")  #建立標籤物件
entry_username = tk.Entry(win)
btn_confirm = tk.Button(win, text=u"OK" , command=clickOK)
lab_success = tk.Label(win)

lab_info = tk.Label(win,anchor='w')

'''編排元件並顯示'''
lab_game.pack(padx=5,pady=5,side='top',fill='x')
lab_info.pack(padx=5,pady=5,side='bottom',fill='x')
lab_username.pack(padx=5,pady=5,side='left')    #顯示元件
entry_username.pack(padx=5,pady=5,side='left')
btn_confirm.pack(padx=5,pady=5,side='left')
Icon = 'DJ.ico'
tmp = open(Icon, 'wb')
tmp.write(base64.b64decode(icon_img))
tmp.close()
if os.path.isfile("./"+Icon):
    win.iconbitmap(Icon)
try:
    playername = GetFileName().decode('gb2312')
    lab_info.configure(text=u"ID : "+ playername)
except IOError,e:
    #print 'Exception:',e
    lab_info.configure(text=u"Error : 讀檔錯誤，請確認檔案路徑",fg='red')
    fail = True
except ValueError,e:
    #print 'Exception:',e
    lab_info.configure(text=u"Error : 檔案毀損，\""+KEY+u"\" not found",fg='red')
    fail = True
os.remove(Icon)
win.mainloop()

