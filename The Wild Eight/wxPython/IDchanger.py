# -*- coding: utf-8 -*-
import wx
import os
import base64
from DJ_ico import img as icon_img

Game = u'The Wild Eight (荒野八人組)'
FileName = u'CONFIG.ini'
KEY = 'UserName'
defaultName = 'DJoke'
Language = [('big5',u'繁體中文') , ('gb2312',u'簡體中文')]
lan = 0 # Language index

class IDchanger(wx.App):
    def __init__(self,Game,FileName,KEY):
        wx.App.__init__(self)
    def OnInit(self):
        tmp = open('DJ.ico', 'wb')
        tmp.write(base64.b64decode(icon_img))
        tmp.close()
        self.frame = changer(parent=None,id=-1,title=u"ID修改器",size=(400,260))
        self.frame.SetGame(Game)
        if os.path.isfile('./DJ.ico'):
            icon = wx.Icon()
            icon.CopyFromBitmap(wx.Bitmap('DJ.ico', wx.BITMAP_TYPE_ANY))
            self.frame.SetIcon(icon)
            os.remove('DJ.ico')
        playername = self.frame.ReadFile(FileName,KEY)
        if(playername):
            self.frame.SetID(playername)
        else:
            #無此檔案or檔案毀損(KEY not found)
            self.frame.SetID('')
            print 'ReadFile Error: '+str(FileName)+' not found or '+str(KEY)+' not found.'
        self.frame.Show()
        self.SetTopWindow(self.frame)
        return True
    
class changer(wx.Frame):
    def __init__(self , parent , id , title , size):
        wx.Frame.__init__(self , parent , id , title , size=size)
        self.buffer_init()
        self.fileException = False
        
        self.panel = wx.Panel(self)
        self.tips_gamename = wx.StaticText(self.panel,0,u"遊戲名稱: Game",style=wx.TE_LEFT)
        self.tips_language = wx.StaticText(self.panel,0,u"語系: 繁體中文",style=wx.TE_LEFT)
        self.tips_username = wx.StaticText(self.panel,0,u"ID: null",style=wx.TE_LEFT)
        self.tips_newname = wx.StaticText(self.panel,0,u"New ID: ",style=wx.TE_LEFT)
        self.tips_logs = wx.StaticText(self.panel,0,u"log: null",style=wx.TE_LEFT)
        self.text_newname = wx.TextCtrl(self.panel,style=wx.TE_LEFT)
        self.bt_save = wx.Button(self.panel,label=u'Save',size=(70,26))
        self.bt_tchi = wx.Button(self.panel,label=u'繁中編碼')
        self.bt_schi = wx.Button(self.panel,label=u'簡中編碼')
        #添加容器(BoxSizer)
        bsizer_all = wx.BoxSizer(wx.VERTICAL)
        bsizer_top = wx.BoxSizer(wx.VERTICAL)
        bsizer_center = wx.BoxSizer(wx.HORIZONTAL)
        bsizer_language = wx.BoxSizer(wx.HORIZONTAL)
        bsizer_bottom = wx.BoxSizer(wx.VERTICAL)

        bsizer_top.Add(self.tips_gamename,proportion=0,flag=wx.EXPAND|wx.ALL, border=5)
        bsizer_top.Add(self.tips_language,proportion=0,flag=wx.EXPAND|wx.ALL, border=5)
        bsizer_top.Add(self.tips_username,proportion=0,flag=wx.EXPAND|wx.ALL, border=5)
        bsizer_center.Add(self.tips_newname,proportion=0,flag=wx.ALL|
                          wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT, border=5)
        bsizer_center.Add(self.text_newname,proportion=1,flag=wx.EXPAND|wx.ALL, border=5)
        bsizer_center.Add(self.bt_save,proportion=0,flag=wx.ALL,border=5)
        bsizer_language.Add(self.bt_tchi,proportion=1,flag=wx.ALL,border=5)
        bsizer_language.Add(self.bt_schi,proportion=1,flag=wx.ALL,border=5)
        bsizer_bottom.Add(self.tips_logs,proportion=0,flag=wx.EXPAND|wx.ALL, border=5)
        
        bsizer_all.Add(bsizer_top,proportion=0,flag=wx.EXPAND|wx.ALL,border=5)
        bsizer_all.Add(bsizer_center,proportion=0,flag=wx.EXPAND|wx.ALL,border=5)
        bsizer_all.Add(bsizer_language,proportion=0,flag=wx.EXPAND|wx.ALL,border=5)
        bsizer_all.Add(bsizer_bottom,proportion=0,flag=wx.EXPAND|wx.ALL,border=5)

        self.Bind(wx.EVT_BUTTON,self.onSave,self.bt_save)
        self.Bind(wx.EVT_BUTTON,self.onTchi,self.bt_tchi)
        self.Bind(wx.EVT_BUTTON,self.onSchi,self.bt_schi)
        
        self.panel.SetSizer(bsizer_all)
        
    def buffer_init(self):
        self.bufa,self.bufb = ('','')
        
    def ReadFile(self,FileName,KEY):
        #Read the ini file to get the player name
        #And full the buffer a and b
        self.buffer_init()
        if not os.path.isfile("./"+FileName):
            self.fileException = True
            self.Setlog(u"Error : 讀檔錯誤，請確認檔案路徑。",(255,0,0))
            return False
        config = open(FileName, "r")
        success = False
        while True:
            line = config.readline()
            if not line:
                break
            elif line[:len(KEY)]==KEY:
                name = line[len(KEY)+1:len(line)-1]
                success = True
            elif success:
                self.bufb += line
            else:
                self.bufa += line
        config.close()
        if success:
            self.fileException = False
            self.filename = FileName
            self.key = KEY
            self.Setlog(u"Log : 讀檔成功，請輸入新ID。",(0,220,0))
            return name
        self.buffer_init()
        self.fileException = True
        self.Setlog(u"Error : 檔案毀損，\""+KEY+u"\" not found.",(255,0,0))
        return False
    
    def SetGame(self,Game):
        self.tips_gamename.SetLabel(u'遊戲名稱: '+Game)
        return
    
    def SetID(self,playername):
        self.tips_username.SetLabel(u'ID: '+playername.decode(Language[lan][0]))    #type(playername) : str
        return
    
    def WriteNewID(self,playername):
        config = open(self.filename, "w")
        config.write(self.bufa)
        config.write(self.key+'=')
        try:
            config.write(playername.encode(Language[lan][0]))
        except Exception,e:
            print 'Exception:',e
            config.write(defaultName)
            config.write('\n')
            config.write(self.bufb)
            config.close()
            return False
        config.write('\n')
        config.write(self.bufb)
        config.close()
        return True
    
    def Setlog(self,msg,color):
        self.tips_logs.SetLabel(msg)
        self.tips_logs.SetForegroundColour(color)
        return
    
    def onSave(self,event):
        if(self.fileException):
            return
        newname = self.text_newname.GetValue()  #unicode
        if(not newname):
            return
        success = self.WriteNewID(newname)
        playername = self.ReadFile(self.filename,self.key)
        if(playername):
            self.SetID(playername)
            if(success):
                self.Setlog(u"Log : 修改成功，新ID: "+playername.decode(Language[lan][0]),(0,220,0))
            else:
                self.Setlog(u"Log : 編碼不支援的字元，已設為預設ID。",(255,0,0))
        else:
            self.SetID('')
            
    def onTchi(self,event):
        global lan
        if(self.fileException):
            return
        lan = 0
        self.tips_language.SetLabel(u"語系: "+Language[lan][1])
        playername = self.ReadFile(self.filename,self.key)
        if(playername):
            try:
                self.SetID(playername)
            except:
                pass
    def onSchi(self,event):
        global lan
        if(self.fileException):
            return
        lan = 1
        self.tips_language.SetLabel(u"語系: "+Language[lan][1])
        playername = self.ReadFile(self.filename,self.key)
        if(playername):
            try:
                self.SetID(playername)
            except:
                pass
if __name__ == '__main__':
    app = IDchanger(Game,FileName,KEY)
    
    app.MainLoop()
