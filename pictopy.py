# -*- coding: utf-8 -*-
import base64
 
def pic2py(pic):
    """
    將圖片轉為py檔
    """
    open_pic = open("%s" % pic, 'rb')
    b64str = base64.b64encode(open_pic.read())
    open_pic.close()
    #b64str一定要加.decode()
    write_data = 'img = "%s"' % b64str.decode()
    f = open('%s.py' % pic.replace('.', '_'), 'w+')
    f.write(write_data)
    f.close()
 
if __name__ == '__main__':
    pics = ["DJ.ico"]
    for i in pics:
        pic2py(i)
